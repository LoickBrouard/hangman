package hangman

import (
	"bufio"
	"errors"
	"fmt"
	"hangman/datas"
	"os"
	"strings"
)

var reader = bufio.NewReader(os.Stdin)

func ReadGuess()(guess string, err error){
	fmt.Print("Choisissez une lettre : ")
	guess, err = reader.ReadString('\n') // Lecture jusqu'a ce que l'utilisateur tape "entrée"
	if err != nil{
		return guess, err
	}
	guess = strings.ToUpper(strings.TrimSpace(guess))
	switch{
	case !IsInArrayString(guess, datas.Alphabet):
		err = errors.New("J’ai passé ma vie à essayer de ne pas faire d’erreurs… Ta proposition en est une par contre\n")
		break
	default:
		break
	}
	return guess, err
}

func ReadMenu(catchPhrase string, list []string)(choice string, err error){
	fmt.Print(catchPhrase)
	choice, err = reader.ReadString('\n') // Lecture jusqu'a ce que l'utilisateur tape "entrée"
	if err != nil{
		return choice, err
	}
	choice = strings.ToUpper(strings.TrimSpace(choice))
	switch{
	case !IsInArrayString(choice, list):
		err = errors.New("J’ai passé ma vie à essayer de ne pas faire d’erreurs… Ta proposition en est une par contre\n")
		break
	default:
		break
	}
	return choice, err
}