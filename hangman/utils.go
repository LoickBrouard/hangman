package hangman

import (
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

func IsInArrayString(value string, arrayString []string) bool{
	for _,elt := range arrayString{
		if strings.ToLower(elt) == strings.ToLower(value){
			return true
		}
	}
	return false
}

/*
Ask for user input
First parameter : Question string
Second parameter : Authorized answers
Return the choice
*/
func AskForInput(text string, choice []string)string{
	for true{
		fmt.Printf("%s\n", text)
		userChoice := ""
		fmt.Scanln(&userChoice)
		isMatch, index := IsMatchOneTime(strings.ToLower(userChoice), choice)
		if isMatch && index >= 0{
			return choice[index]
		} else{
			fmt.Println("Vos choix de réponse sont : %v", choice)
		}
	}
	return ""
}

/*
Ask for user input int only
First parameter : Question string
Return the choice
*/
func AskForInputInt(text string, )int{
	fmt.Printf("%s\n", text)
	userChoice := ""
	fmt.Scanln(&userChoice)
	result, err := strconv.ParseInt(userChoice, 10, 0)
	switch err {
	case nil :
		return int(result)
	default:
		fmt.Println("Nombre non reconu, par défaut le nombre d'essai est de 5")
		return 5
	}
}

/*
Check if a searched element is in the selected array
Return true or false for the match  and the index of the searched element
*/
func IsMatchOneTime(searchedElement string, array []string)(bool, int){
	for index, value := range array {
		if searchedElement == strings.ToLower(value){
			return true, index
		}
	}
	return false, 0
}

/*
Check if a searched element is in the selected array
Return true or false for the match  and n array of the indexes of the searched element
*/
func IsMatchSeveralTimes(searchedElement string, array []string)(b bool, indexes []int){
	find := false
	for index, value := range array {
		if searchedElement == strings.ToLower(value){
			find = true
			indexes = append(indexes, index)
		}
	}
	if find{
		return true, indexes
	}
	return false, indexes
}
func ClearScreen(){
	cmd := exec.Command("cmd", "/c", "cls")
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func GenerateRandomItem(list []string) string{
	rand.Seed(time.Now().UnixNano())
	return  list[rand.Intn(len(list))]
}