package hangman

import (
	"hangman/datas"
)

func Play(playList []string)string {
	message := ""
	Letters := GenerateRandomWord(playList)
	g:= New(datas.MaxTries, Letters)
	for g.State == ""{
		RemainingscreenGame(g.TurnsLeft, g.FoundLetters, g.TurnsLeft, g.UsedLetters, message)
		letter, err := ReadGuess()
		switch err{
		case nil :
			message = GuessTreatment(letter, g)
			break
		default:
			message = "(" + letter + ") Erreur :" + err.Error()
			break
		}
		switch {
		case !IsInArrayString(" _ ", g.FoundLetters):
			g.State = "Gagne"
			break
		case g.TurnsLeft == 0:
			g.State = "Perdu"
			break
		}
	}
	return CheckEndGame(g)
}
