package hangman

import (
	"fmt"
	"strings"
	"time"
)

func RemainingscreenMenu(menuType string, message ...string){
	ClearScreen()
	DrawWelcome()
	switch menuType{
	case "Main":
		DrawMenu(message[0])
		break
	case "Param":
		DrawMenuParam(message[0])
		break
	case "Difficulty":
		DrawMenuDifficulty(message[0])
		break
	}
}

func RemainingscreenGame(turnsLeft int, mot []string,tries int, lettersUsed []string, message ...string){
	ClearScreen()
	DrawWelcome()
	fmt.Println("")
	drawTurns(turnsLeft)
	fmt.Println("")
	DrawTitle(mot, tries, lettersUsed, message[0])
}

func YouWon(word string){
	ClearScreen()
	DrawWelcome()
	DrawYouWon()
	fmt.Println("Le mot était bien : " + word + "\n")
}

func YouLoose(word string){
	ClearScreen()
	DrawWelcome()
	DrawYouLoose()
	fmt.Println("Le mot était : " + word + "\n")
}

func EndGame(){
	ClearScreen()
	Goodbye()
	time.Sleep(time.Duration(5)*time.Second)
}

// Fonction qui affiche la bannière d'accueil
// http://patorjk.com/software/taag/#p=display&f=Big&t=PENDU
func DrawWelcome(){
	fmt.Println(`
██████╗ ███████╗███╗   ██╗██████╗ ██╗   ██╗ 
██╔══██╗██╔════╝████╗  ██║██╔══██╗██║   ██║ 
██████╔╝█████╗  ██╔██╗ ██║██║  ██║██║   ██║ 
██╔═══╝ ██╔══╝  ██║╚██╗██║██║  ██║██║   ██║ 
██║     ███████╗██║ ╚████║██████╔╝╚██████╔╝ 
╚═╝     ╚══════╝╚═╝  ╚═══╝╚═════╝  ╚═════╝
                                     CINEMA
    `)
}

func DrawTitle(mot []string, tries int, lettersUsed []string, message ...string) {
	fmt.Println("Mot à deviner : ", mot)
	fmt.Println("Essais restants : ", tries)
	fmt.Println("Lettres jouées : ", strings.Join(lettersUsed, ","))
	if len(message) != 0{
		fmt.Println("")
		fmt.Println(message[0])
	}

}
func DrawMenu(message ...string) {
	fmt.Println("1 . Jouer")
	fmt.Println("2 . Paramètres")
	fmt.Println("3 . Quitter")
	if len(message) != 0{
		fmt.Println("")
		fmt.Println(message[0])
	}

}

func DrawMenuParam(message ...string) {
	fmt.Println("1 . Choix du niveau")
	fmt.Println("2 . Nombre d'essai")
	fmt.Println("3 . Retour au menu")
	if len(message) != 0{
		fmt.Println("")
		fmt.Println(message[0])
	}
}

func DrawMenuDifficulty(message ...string) {
	fmt.Println("1 . Classique (Un pendu avec des mots)")
	fmt.Println("2 . Cinéphile (Top 100 films)")
	fmt.Println("3 . Irréalisable (Spécial réalisateurs)")
	fmt.Println("4 . Productif (Spécial producteurs)")
	if len(message) != 0{
		fmt.Println("")
		fmt.Println(message[0])
	}
}

// Fonction qui dessine l’échaffaud qui attend le nombe d'essai restants
func drawTurns(l int) {
	var draw string
	switch l {
	case 0:
		draw = `
    ____
   |    |      
   |    o      
   |   /|\     
   |    |
   |   / \
  _|_
 |   |______
 |          |
 |__________|
        `
	case 1:
		draw = `
    ____
   |    |      
   |    o      
   |   /|\     
   |    |
   |    
  _|_
 |   |______
 |          |
 |__________|
        `
	case 2:
		draw = `
    ____
   |    |      
   |    o      
   |    |
   |    |
   |     
  _|_
 |   |______
 |          |
 |__________|
        `
	case 3:
		draw = `
    ____
   |    |      
   |    o      
   |        
   |   
   |   
  _|_
 |   |______
 |          |
 |__________|
        `
	case 4:
		draw = `
    ____
   |    |      
   |      
   |      
   |  
   |  
  _|_
 |   |______
 |          |
 |__________|
        `
	case 5:
		draw = `
    ____
   |        
   |        
   |        
   |   
   |   
  _|_
 |   |______
 |          |
 |__________|
        `
	case 6:
		draw = `
    
   |     
   |     
   |     
   |
   |
  _|_
 |   |______
 |          |
 |__________|
        `
	case 7:
		draw = `
 





  _ _
 |   |______
 |          |
 |__________|
        `
	case 8:
		draw = `










        `
	}
	fmt.Println(draw)
}

func DrawYouWon(){
	draw := `
                                   .''.       
       .''.      .        *''*    :_\/_:     . 
      :_\/_:   _\(/_  .:.*_\/_*   : /\ :  .'.:.'.
  .''.: /\ :   ./)\   ':'* /\ * :  '..'.  -=:o:=-
 :_\/_:'.:::.    ' *''*    * '.\'/.' _\(/_'.':'.'
 : /\ : :::::     *_\/_*     -= o =-  /)\    '  *
  '..'  ':::'     * /\ *     .'/.\'.   '
      *            *..*         :
 	   *
        *
"It’s gonna be legen… wait for it… dary!!"

Bravo !!
`
fmt.Println(draw)
}

func DrawYouLoose(){
	draw := `
                 uuuuuuu
             uu$$$$$$$$$$$uu
          uu$$$$$$$$$$$$$$$$$uu
         u$$$$$$$$$$$$$$$$$$$$$u
        u$$$$$$$$$$$$$$$$$$$$$$$u
       u$$$$$$$$$$$$$$$$$$$$$$$$$u
       u$$$$$$$$$$$$$$$$$$$$$$$$$u
       u$$$$$$"   "$$$"   "$$$$$$u
       "$$$$"      u$u       $$$$"
        $$$u       u$u       u$$$
        $$$u      u$$$u      u$$$
         "$$$$uu$$$   $$$uu$$$$"
          "$$$$$$$"   "$$$$$$$"
            u$$$$$$$u$$$$$$$u
             u$"$"$"$"$"$"$u
  uuu        $$u$ $ $ $ $u$$       uuu
 u$$$$        $$$$$u$u$u$$$       u$$$$
  $$$$$uu      "$$$$$$$$$"     uu$$$$$$
u$$$$$$$$$$$uu    """""    uuuu$$$$$$$$$$
$$$$"""$$$$$$$$$$uuu   uu$$$$$$$$$"""$$$"
 """      ""$$$$$$$$$$$uu ""$"""
           uuuu ""$$$$$$$$$$uuu
  u$$$uuu$$$$$$$$$uu ""$$$$$$$$$$$uuu$$$
  $$$$$$$$$$""""           ""$$$$$$$$$$$"
   "$$$$$"                      ""$$$$""
     $$$"                         $$$$"
`
	fmt.Println(draw)
}

func Goodbye(){
	goodbye := `
        >\\\|/<
        |_"""_|
        (O) (o)
+----OOO--(_)--OOOo-----=| "Tu fais comme dans l’infanterie, tu te tires ailleurs !"|=----+
`
	fmt.Println(goodbye)
}