package hangman

import (
	"hangman/datas"
	"math/rand"
	"sort"
	"strings"
	"time"
)

type Game struct{
	State string // Etat du jeu (loss, win, ...)
	Letters []string // Le mot à devner
	FoundLetters []string // l'affichage du mot avec les lettres trouvées "B _ _ _ _ "
	UsedLetters []string // Les lettres jouées
	TurnsLeft int // Nombre de tour restant
}

func New(turns int, Letters []string) *Game{

	// Create a table with " _ " for each letter to find
	found := make([]string, len(Letters))
	for i,letter := range Letters{
		if(IsInArrayString(letter, datas.Alphabet)) {
			found[i] = " _ "
		} else{
			found[i] = letter
		}

	}

	// On return le jeu
	return &Game{
		State: "",
		Letters: Letters,
		FoundLetters: found,
		UsedLetters: []string{},
		TurnsLeft: turns,
	}
}

func GuessTreatment(guess string, g *Game)  (string) {
	message := ""
	switch IsInArrayString(guess, g.UsedLetters){
	case true:
		message = "Il y a un bug dans la matrice, lettre déjà vue"
		break
	case false:
		switch IsInArrayString(guess, g.Letters){
		case true:
			message = "Bien vu ;)"
			for i,_ := range g.FoundLetters{
					if g.Letters[i] == guess{
						g.FoundLetters[i] = guess
					}
				}
			g.UsedLetters = append(g.UsedLetters, guess)
			break
		case false:
			message = GenerateRandomItem(datas.EchecsPunchLines)
			g.UsedLetters = append(g.UsedLetters, guess)
			sort.Strings(g.UsedLetters)
			g.TurnsLeft -= 1
			break
			}
	}
	return message
}

func CheckEndGame (g *Game)string{
	if g.State == "Gagne"{
		ClearScreen()
		YouWon(strings.Join(g.Letters, ""))
		switch AskForInput("Bien joué... T’es pas mouru l’âne, t’es pas mouru.. Tu rejoues ? (O/N)", []string{"O", "N"}){
		case "O":
			return "menu"
			break
		case "N":
			return "quit"
			break

		}
	}
	if g.State == "Perdu"{
		ClearScreen()
		YouLoose(strings.Join(g.Letters, ""))
		switch AskForInput("Il vaut mieux s'en aller la tête basse que les pieds devant. Tu rejoues ? (O/N)", []string{"O", "N"}){
		case "O":
			return "menu"
			break
		case "N":
			return "quit"
			break
		default:
			break
		}
	}
	return "quit"
}

// Generate a random word within the list
// Convert it in an array of string
// Create the Current guess with an array of same length filled with " _ "
func GenerateRandomWord(list []string) []string {

	rand.Seed(time.Now().UnixNano())
	return  strings.Split(list[rand.Intn(len(list))],"")
}