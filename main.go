package main

import (
	"hangman/datas"
	"hangman/hangman"
	"hangman/menus"
	"os"
)

var next = "menu"
var message = ""
var playList []string

func main(){
	for next != "quit"{
		switch next{
		case "menu":
			next, message = menus.MainMenu(message)
			break
		case "parameters":
			next, message = menus.ParametersMenu(message)
			break
		case "play":
			switch(datas.ListChosen){ // Classic, Cinephile, Irrealisable, Productif
			case "Classic":
				playList = datas.Classic
				break
			case "Cinephile":
				playList = datas.Cinephile
				break
			case "Irrealisable":
				playList = datas.Irrealisable
				break
			default:
				playList = datas.Classic
				break
			}
			next = hangman.Play(playList)
			break
		}
	}
	hangman.EndGame()
	os.Exit(0)
}

