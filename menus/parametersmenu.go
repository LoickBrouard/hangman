package menus

import (
	"errors"
	"fmt"
	"hangman/datas"
	"hangman/hangman"
	"strconv"
)

func ParametersMenu(message string ) (string, string){
	choiceMenu := ""
	err := errors.New("")
	for choiceMenu != "menu"{
		hangman.RemainingscreenMenu("Param", message)
		choiceMenu, err = hangman.ReadMenu("Gagnez une situation sans victoire en réécrivant les règles.\nChoisi un numéro : ", []string{"1","2","3"})
		switch err{
		case nil :
			switch choiceMenu{
			case "1": // 1 . Choix du niveau
				choiceMenu, message = DifficultiesMenu(message)
			case "2": // 2 . Nombre d'essai
				hangman.RemainingscreenMenu("Param", "En combien tu veux perdre ?")
				userChoice := ""
				fmt.Scanln(&userChoice)
				result, err := strconv.ParseInt(userChoice, 10, 0)
				switch err {
				case nil :
					datas.MaxTries = int(result)
					message = "Ok va pour " + strconv.Itoa(datas.MaxTries) + " essais"
					break
				default:

					datas.MaxTries = 10
					break
				}
			break

			case "3": // 3 . Retour au menu
				choiceMenu =  "menu"
				message = ""
				break
			}
			break
		default:
			message = "(" + choiceMenu + ") Erreur :" + err.Error()
			choiceMenu = "menu"
		}
	}

	return choiceMenu, message
}