package menus

import "hangman/hangman"

func MainMenu(message string ) (string, string){
	hangman.RemainingscreenMenu("Main", message)
	choice, err := hangman.ReadMenu("Mais dis-donc, on est tout de même pas venu pour beurrer les sandwichs !?\nChoisi un numéro : ", []string{"1", "2","3"})
	switch err{
	case nil :
		switch choice{
		case "1": // 1 . Jouer
			choice =  "play"
		case "2": // 2 . Paramètres"
			choice =  "parameters"
		case "3": // 3 . Quitter
			choice =  "quit"
		}
		break
	default:
		message = "(" + choice + ") Erreur :" + err.Error()
		choice = "menu"
	}
	return choice, message
}