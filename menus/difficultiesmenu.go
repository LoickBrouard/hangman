package menus

import (
	"errors"
	"hangman/datas"
	"hangman/hangman"
)

func DifficultiesMenu(message string ) (string, string){
	choiceMenu := ""
	err := errors.New("")
	for choiceMenu != "menu"{
		hangman.RemainingscreenMenu("Difficulty", message)
		choiceMenu, err = hangman.ReadMenu("Vous voulez la jouer soft, je suis pas contrariant. Vous voulez la jouer hard… on va la jouer hard.\nChoisi un numéro : ", []string{"1","2","3","4","5"})
		switch err{
		case nil :
			switch choiceMenu{
			case "1": // 1 . Classique (Un pendu quoi)
				datas.ListChosen = "Classic"
				break
			case "2": // 2 . Cinéphile (Que du 7eme Art)
				datas.ListChosen = "Cinephile"
				break
			case "3": // 3 . Irréalisable (Spécial réalisateurs)
				datas.ListChosen = "Irréalisable"
				break
			case "4": // 4 . Productif (Spécial producteurs)
				break
			}
			choiceMenu = "menu"
			break
		default:
			message = "(" + choiceMenu + ") Erreur :" + err.Error()
			choiceMenu = "menu"
		}
	}
	return choiceMenu, message
}